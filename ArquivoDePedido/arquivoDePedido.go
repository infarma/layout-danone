package ArquivoDePedido

import (
	"fmt"
	"strings"
)

//func Cabecalho(versaoLayout string, cpfOrCnpj string, codCliente string, codPedido string, dataPedido string, horaPedido string) string {
	func Cabecalho(versaoLayout string, cpfOrCnpj string, codCliente string, codPedido string, dataPedido string, horaPedido string,frete float64,payment string) string {
	cabecalho := fmt.Sprint("01")
	cabecalho += fmt.Sprintf("%-6s", versaoLayout)
	cabecalho += fmt.Sprintf("%-14s", cpfOrCnpj)
	cabecalho += fmt.Sprintf("%06s", codCliente)
	cabecalho += fmt.Sprintf("%011s", codPedido)
	// CNPJ Da Distribuidor (DANONE)
	// cabecalho += fmt.Sprint("23643315000152")
	cabecalho += fmt.Sprintf("%08s", dataPedido)
	cabecalho += fmt.Sprintf("%04s", horaPedido)
	cabecalho += fmt.Sprintf("%09s", strings.Replace(fmt.Sprintf("%.2f", frete), ".", "", 1))
	if payment == "Cartão de Crédito"{
		cabecalho += fmt.Sprintf("CMG")
	} else {
		cabecalho += fmt.Sprintf("DUP")
	}
	return cabecalho
}

func Mensagem(retira bool ,Des_Endereco,Num_Endereco,Cpl_Endereco,Des_Bairro,cidade,estado,cep string) string{
	//var mensagem string
	if retira == true{
		mensagem := fmt.Sprintf("%-40s", "02CLIENTE RETIRA NA DISTRIBUIDORA")
		return mensagem
	}else{
		mensagem := fmt.Sprint("02")
		mensagem += fmt.Sprint(Des_Endereco+",")
		mensagem += fmt.Sprint(Num_Endereco+",")
		mensagem += fmt.Sprint(Cpl_Endereco+",")
		mensagem += fmt.Sprint(Des_Bairro+",")
		mensagem += fmt.Sprint(cidade+",")
		mensagem += fmt.Sprint(estado+",")
		mensagem += fmt.Sprint(cep)
		return mudaCaracterEspecial(mensagem)
	}

		//mensagem += fmt.Sprintf("%-40s", "02ENT:"+enderecoEntrega)
	//return mensagem
}

func mudaCaracterEspecial(s string) string {
	j := strings.ToUpper(s)
	j = strings.Replace(j, "Â", "A", -1)
	j = strings.Replace(j, "À", "A", -1)
	j = strings.Replace(j, "Ã", "A", -1)
	j = strings.Replace(j, "Á", "A", -1)
	j = strings.Replace(j, "Ê", "E", -1)
	j = strings.Replace(j, "É", "E", -1)
	j = strings.Replace(j, "Í", "I", -1)
	j = strings.Replace(j, "Ô", "O", -1)
	j = strings.Replace(j, "Õ", "O", -1)
	j = strings.Replace(j, "Ó", "O", -1)
	j = strings.Replace(j, "Û", "U", -1)
	j = strings.Replace(j, "Ú", "U", -1)
	j = strings.Replace(j, "Ü", "U", -1)
	j = strings.Replace(j, "Ç", "C", -1)
	return j
}

//func Detalhe(codProduto string, quantidade string) string {
	func Detalhe(ean string, quantidade string,precoOriginal float64,precoUnitario float64, percentualDesconto float64) string {
	detalhe := fmt.Sprint("03")
	detalhe += fmt.Sprintf("%-1s", "2")
	detalhe += fmt.Sprintf("%013s", ean)
	detalhe += fmt.Sprintf("%07s", quantidade)
	detalhe += fmt.Sprintf("%02s", "")
	detalhe += fmt.Sprintf("%09s", strings.Replace(fmt.Sprintf("%.2f", precoOriginal), ".", "", 1))
	detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.8f", precoUnitario), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", percentualDesconto), ".", "", 1))

	return detalhe
}

func Rodape(totalItens string, totalUnidades string) string {

	rodape := fmt.Sprint("09")
	rodape += fmt.Sprintf("%04s", totalItens)
	rodape += fmt.Sprintf("%08s", totalUnidades )
	rodape += fmt.Sprintf("%02s", "")

	return rodape
}

func preencherComZero(tamanho int, tamanhoString int) string {
	var zerosString string
	for i := 0; i <(tamanho - tamanhoString); i++ {
		zerosString += "0"
	}
	return zerosString
}

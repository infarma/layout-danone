package BuscaDePedido

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"pedido-eletronico/app/utils"
	"strconv"
	"strings"
)

//const FIELD_VALUE = "processing"

type Order struct {
	// Campos do De-Para
	CustomerEmail        string         `json:"customer_email"`      // Email
	CustomerFirstName    string         `json:"customer_firstname"`  // firstname + lastname = Razao_Social
	CustomerLastName     string         `json:"customer_lastname"`   // firstname + lastname = Razao_Social
	CustomerId           int            `json:"customer_id"`         // Código
	CustomerTaxVat       string         `json:"customer_taxvat"`     // Cgc_cpf
	BillingAddress       BillingAddress `json:"billing_address"`
	ExtensionAttributes	 ExtensionAttributes		    	`json:"extension_attributes"`
	// Campos para o layout
	OrderId              int     `json:"entity_id"`
	CreatedAt            string  `json:"created_at"`
	BaseSubTotal         float64  `json:"base_subtotal"`
	BaseSubTotalInclTax  float64  `json:"base_subtotal_incl_tax"`
	BaseSubTotalInvoiced float64  `json:"base_subtotal_invoiced"`
	BaseTaxAmount        float64  `json:"base_tax_amount"`
	BaseTaxInvoiced      float64  `json:"base_tax_invoiced"`
	BaseTotalInvoiced    float64   `json:"base_total_invoiced"`
	BaseTotalPaid        float64   `json:"base_total_paid"`
	TotalItemCount       int       `json:"total_item_count"`
	TotalQtyOrdered      int       `json:"total_qty_ordered"`
	TotalInvoiced        float64   `json:"total_invoiced"`
	Status               string    `json:"status"`
	State                string    `json:"state"`
	Items                []Item    `json:"items"`
	Frete				float64    `json:"base_shipping_invoiced"`
	Desconto 	        float64    `json:"base_discount_invoiced"`
	ShippingDescription string     `json:"shipping_description"`
	// Campo para atualizacao do pedido
	IncrementId          string    `json:"increment_id"`
	StatusHistories      []StatusHistory `json:"status_histories"`


}

type ExtensionAttributes struct{
	ShippingAssignments []ShippingAssignments `json:"shipping_assignments"`
	PaymentAdditionalInfo []PaymentAdditionalInfo `json:"payment_additional_info"`
	Telephone string  `json:"customer_telephone"`
	Telephone2 string `json:"customer_cellphone"`
}

type PaymentAdditionalInfo struct{
	Key string `json:"key"`
	Value string `json:"value"`
}

type StatusHistory struct{
	ParentId   int      `json:"parent_id"`
}

type ShippingAssignments struct{
	Shipping Shipping `json:"shipping"`
}

type Shipping struct{
	Address Address `json:"address"`
}

type Address struct{
	City      string   `json:"city"`
	PostCode  string   `json:"postcode"`
	Region    string   `json:"region"`
	RegionId  string   `json:"region_code"`
	Street    []string `json:"street"`
}

type Item struct {
	Discount      float64   `json:"discount_percent"`
	ProductId     int     `json:"product_id"`
	ItemId        int     `json:"item_id"`
	ProductSku    string  `json:"sku"`
	EanProduct    string  `json:"ean_product"`
	Name          string  `json:"name"`
	Description   string  `json:"description"`
	IsQtyDecimal  bool    `json:"is_qty_decimal"`
	OrderId       int     `json:"order_id"`
	OriginalPrice float64 `json:"original_price"`
	Price         float64 `json:"price"`
	PriceInclTax  float64 `json:"price_incl_tax"`
	QtyInvoiced   int     `json:"qty_invoiced"`
	QtyOrdered    int     `json:"qty_ordered"`
	QtyShipped    int     `json:"qty_shipped"`
	CreatedAt     string  `json:"created_at"`
	ProductType string   `json:"product_type"`
}

type BillingAddress struct {
	City      string   `json:"city"`
	PostCode  string   `json:"postcode"`
	Region    string   `json:"region"`
	RegionId  string   `json:"region_code"`
	Street    []string `json:"street"`
	ParentId  int      `json:"parent_id"`
}

type Products struct {
	CustomAttributes []CustomAttr `json:"custom_attributes"`
}

type CustomAttr struct {
	AttributeCode string `json:"attribute_code"`
	Value         string `json:"value"`
}

func BuscarPedidos(pageSize int, url, token string,field_value string) []Order {
	//var IndexRemovidoSemEAN []int
	var IndexComEAN []int
	resp := FazerRequisicao("orders", "status", field_value, "eq", pageSize, url, token)

	binData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	var orders []Order
	var hasEanOrders []Order
	var isEqual bool = false
	var hasEan bool = true
	rawData := string(binData)
	rawData = rawData[9 : strings.Index(rawData, "search_criteria")-2]
	json.Unmarshal([]byte(rawData), &orders)

	for orderKey, order := range orders {
		hasEan = true
		for itemKey, item := range order.Items {
			ean := BuscarEAN(item.ProductSku, url, token)
			if ean == "não encontrado" {
				//if (len(IndexRemovidoSemEAN) == 0 || !(IndexRemovidoSemEAN[len(IndexRemovidoSemEAN)-1] == orderKey)) {
					hasEan = false
					//IndexRemovidoSemEAN = append(IndexRemovidoSemEAN, orderKey)
					fmt.Println("Error: Pedido de increment ID", order.IncrementId, "com itens sem EAN cadastrado, de SKU :" ,order.Items[itemKey].ProductSku)
				//}
			} else {
				orders[orderKey].Items[itemKey].EanProduct = ean
				for _, index := range IndexComEAN {
					if(orderKey == index){
                    isEqual = true;
					}
				}
				if(!isEqual){
					IndexComEAN = append(IndexComEAN, orderKey)
				}
				isEqual = false
			}
			if(itemKey == len(order.Items)-1){
				for i, index := range IndexComEAN {
					if(orderKey == index){
						if(!hasEan){
							IndexComEAN = append(IndexComEAN[:i], IndexComEAN[i+1:]...)
						}
					}
				}
			}
		}
	}
	// REMOVENDO PEDIDOS COM PRODUTOS SEM EAN
	for _, index := range IndexComEAN {
		//orders = append(orders[:index], orders[index+1:]...)
		hasEanOrders = append(hasEanOrders,orders[index])
	}

	return hasEanOrders
}

func BuscarEAN(sku string, url string, token string)  string {
	resp := FazerRequisicao("products", "sku", sku, "eq", 1, url, token)

	binData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	rawData := string(binData)
	var products []Products
	rawData = rawData[9 : strings.Index(rawData, "search_criteria")-2]
	json.Unmarshal([]byte(rawData), &products)

	for _, value := range products {
		for _, value2 := range value.CustomAttributes {
			if value2.AttributeCode == "ean" {
				return value2.Value
			}
		}
	}
	return "não encontrado"
}

func FazerRequisicao(model string, searchField string, searchFieldValue string, searchCondition string, pageSize int, url string, token string) *http.Response {
	client := &http.Client{}

	baseUrl        := url

	moduleUrl      := model
	searchCriteria := "?searchCriteria[filterGroups][0][filters][0][field]=" + searchField + "&searchCriteria[filterGroups][0][filters][0][value]=" + searchFieldValue
	conditionType  := "&searchCriteria[filterGroups][0][filters][0][conditionType]=" + searchCondition
	pagination     := "&searchCriteria[pageSize]=" + strconv.Itoa(pageSize)

	fullUrl        := baseUrl + moduleUrl + searchCriteria + conditionType + pagination

	req, err := http.NewRequest("GET", fullUrl, nil)
	if err != nil {
		fmt.Println(err)
	}

	authorizationToken := "Bearer " + token
	authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)

	req.Header.Add("Authorization", authorizationToken)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	return resp
}


/*********************************
**********************************
Transformar em novos aquivos
**********************************
***********************************/
func TransformarOrderEmTexto(order Order) string {
	conteudo := `01`
	conteudo += strconv.Itoa(order.OrderId)
	conteudo += strconv.Itoa(order.CustomerId)
	conteudo += order.CustomerTaxVat
	conteudo += order.State
	conteudo += order.CreatedAt

	conteudo += `
`
	for _, value := range order.Items {
		conteudo += `02`
		conteudo += strconv.Itoa(value.ProductId)
		conteudo += value.EanProduct
		conteudo += value.Name
		qtyInvString := fmt.Sprintf("%f", value.QtyInvoiced)
		conteudo += qtyInvString
		priceString := fmt.Sprintf("%f", value.Price)
		conteudo += priceString
		conteudo += `
`
	}

	conteudo += `09`
	totalQtyOrdString := strconv.Itoa(order.TotalQtyOrdered)
	conteudo += totalQtyOrdString
	totalItemCountString := strconv.Itoa(order.TotalItemCount)
	conteudo += totalItemCountString
	totalInvoicedString := fmt.Sprintf("%f", order.TotalInvoiced)
	conteudo += totalInvoicedString

	return conteudo
}

func SalvarArquivo(path string, nomeArq string, conteudo string) {
	if utils.VerificaSeDiretorioExiste(path) {
		utils.CriaArquivoNodiretorio(path + "/" + nomeArq, conteudo)
	} else {
		utils.CriaDiretorio(path)
		utils.CriaArquivoNodiretorio(path + "/" + nomeArq, conteudo)
	}
}

func BuscarPedidosStatus(pageSize int, url, token string,status string) []Order {
	resp := FazerRequisicao("orders", "status", status, "eq", pageSize, url, token)

	binData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	var orders []Order
	rawData := string(binData)
	rawData = rawData[9 : strings.Index(rawData, "search_criteria")-2]
	json.Unmarshal([]byte(rawData), &orders)

	return orders
}



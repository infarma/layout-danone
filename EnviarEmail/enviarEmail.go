package EnviarEmail

import (
	"bitbucket.org/infarma/layout-danone/BuscaDePedido"
	"fmt"
	"net/smtp"
	"pedido-eletronico/app/database"
	"pedido-eletronico/app/logger"
	"strconv"
	"strings"
	"time"
)

type smtpServer struct {
	host string
	port string
}
// Address URI to smtp server
func (s *smtpServer) Address() string {
	return s.host + ":" + s.port
}
func EnviarEmail(status, horas,html string,cc []string) {
	// Sender data.
	from := "informativoinfarma@gmail.com"
	password := "paraacripel"
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n";
	subject := "Subject: Aviso de Pedido - Danone! - "+strings.ToUpper(status)+"\r\n"
	smtpServer := smtpServer{host: "smtp.gmail.com", port: "587"}

	message := []byte( subject+mime+html)

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpServer.host)
	// Sending email.
	err := smtp.SendMail(smtpServer.Address(), auth, from, cc, message)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Email Enviado! - "+status)
}

func GerarHtmlProcessing(url,token,status,horas string) string {
	var html string
	color := "0, 0, 0"
	orders := BuscaDePedido.BuscarPedidosStatus(100, url,token,status)
	if len(orders) > 0 {
		for _, order := range orders {
			orderTime,_ := time.Parse("2006-01-02 15:04:05", order.CreatedAt)
			tempo := subtractTime(orderTime,time.Now())
			i, _ := strconv.Atoi(horas)

			if int(tempo) > i {
				if order.IncrementId[0:2]=="19"{
					color = "24, 86, 202"
				}
				if order.IncrementId[0:2]=="20"{
					color = "207, 172, 31"
				}
				if order.IncrementId[0:2]=="21"{
					color = "211, 82, 35"
				}
				//SERVIDOR DE TESTE
				if order.IncrementId[0:2]=="27"{
					color = "128, 0, 128"
				}
				html += "<tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; width: 278px; padding: 10px 0 10px 15px; width: 278px; padding: 10px 0 10px 15px;' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;' > <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; border: 1px solid #e5e5e5; border-radius: 5px; background-color: #ffffff; padding: 12px 15px 15px; width: 253px;' > <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >ID</span ><br /> <span style='color:rgb("+color+")' >"+order.IncrementId+"</span > <br /> <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Order ID</span > <br /> "+strconv.Itoa(order.OrderId)+" </td> </tr> </table> </td> </tr> </table> </td> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; width: 278px; padding: 10px 14px 10px 15px; ' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;' > <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;border: 1px solid #e5e5e5; border-radius: 5px; background-color: #ffffff; padding: 12px 15px 15px; width: 253px;' > <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Data do pedido</span ><br /> "+orderTime.Format("02-01-2006 15:04:05")+" <br /> <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Tempo</span > <br /> "+strconv.Itoa(int(tempo))+" horas atrás  <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' ><a title='Redirecionar para este pedido' href='https://novosabordeviver.com.br/admindsdv/sales/order/view/order_id/"+strconv.Itoa(order.OrderId)+"/' target='_blank' > <img src='https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-16.png' srcset=' https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-16.png 1x, https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-32.png 2x ' alt='external, link icon' height='16' width='12' loading='lazy' /> </a> </span ><br /> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr>"
			}
		}
	}

	return html
}


func GerarHtmlEmSeparacao(url,token,status,horas string) string {
	var html,mensagemProblema string
	color := "0, 0, 0"
	orders := BuscaDePedido.BuscarPedidosStatus(100, url,token,"processing")
	if len(orders) > 0 {
		for _, order := range orders {
			if order.IncrementId[0:2] == "19" {
				color = "24, 86, 202"
			}
			if order.IncrementId[0:2] == "20" {
				color = "207, 172, 31"
			}
			if order.IncrementId[0:2] == "21" {
				color = "211, 82, 35"
			}
			//SERVIDOR DE TESTE
			if order.IncrementId[0:2] == "27" {
				color = "128, 0, 128"
			}

			dataLeitura, err := pegarDataLeituraPedido(order)
			if err != nil {
				fmt.Println(err)
			}
			mensagemOrdemId,hasCodPedCli,err := checarSeExisteCodPedCli(order)
			if err != nil {
				fmt.Println(err)
			}
			if hasCodPedCli {
				mensagemNumPedidoVenda,hasNumeroPedVenda,err := checarNumPedidoVenda(order)
				if err != nil {
					fmt.Println(err)
				}
				if hasNumeroPedVenda{
					mensagemProblema = "Demora na separação"
				} else {
					mensagemProblema = mensagemNumPedidoVenda
				}
			} else {
				mensagemProblema = mensagemOrdemId
			}

			orderTime, _ := time.Parse("2006-01-02 15:04:05", order.CreatedAt)
			tempo := subtractTime(orderTime, time.Now())
			tempoDataLeitura := tempo
			mensagemDataLeitura := ""
			if (dataLeitura == "") {
				mensagemDataLeitura = " "
			} else {
				orderTimeDataLeitura, _ := time.Parse("2006-01-02 15:04:05", dataLeitura)
				mensagemDataLeitura = orderTimeDataLeitura.Format("02-01-2006 15:04:05")
				tempoDataLeitura = subtractTime(orderTimeDataLeitura, time.Now())
			}
				i, _ := strconv.Atoi(horas)
				if int(tempoDataLeitura) >= i {
					html += "<tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; width: 278px; padding: 10px 0 10px 15px; width: 278px; padding: 10px 0 10px 15px;' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;' > <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; border: 1px solid #e5e5e5; border-radius: 5px; background-color: #ffffff; padding: 12px 15px 15px; width: 253px;' > <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >ID</span ><br /> <span style='color:rgb("+color+")' >"+order.IncrementId+"</span > <br /> <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Order ID</span > <br /> "+strconv.Itoa(order.OrderId)+" </td> </tr> </table> </td> </tr> </table> </td> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; width: 278px; padding: 10px 14px 10px 15px; ' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;' > <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;border: 1px solid #e5e5e5; border-radius: 5px; background-color: #ffffff; padding: 12px 15px 15px; width: 253px;' > <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Data do pedido</span ><br /> "+orderTime.Format("02-01-2006 15:04:05")+" <br /> <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Tempo</span > <br /> "+strconv.Itoa(int(tempo))+" horas atrás  <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' ><a title='Redirecionar para este pedido' href='https://novosabordeviver.com.br/admindsdv/sales/order/view/order_id/"+strconv.Itoa(order.OrderId)+"/' target='_blank' > <img src='https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-16.png' srcset=' https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-16.png 1x, https://cdn0.iconfinder.com/data/icons/octicons/1024/link-external-32.png 2x ' alt='external, link icon' height='16' width='12' loading='lazy' /> </a> </span ><br /> </td> </tr> </table> </td> </tr> </table> </td> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px; width: 278px; padding: 10px 14px 10px 15px; ' > <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;' > <table cellspacing='0' cellpadding='0' width='100%' style='border-collapse:separate !important;' > <tr> <td style='font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center; line-height: 21px;border: 1px solid #e5e5e5; border-radius: 5px; background-color: #ffffff; padding: 12px 15px 15px; width: 253px;' > <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Leitura do txt</span ><br /> "+mensagemDataLeitura+" <br /> <span style='font-weight: 700; padding: 5px 0; font-size: 18px; line-height: 1.3; color: #4d4d4d;' >Problema</span > <br /> "+mensagemProblema+" <br /> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr>"
				}
		}
	}

	return html
}



func subtractTime(time1,time2 time.Time) float64{
	diff := time2.Sub(time1).Hours()
	return diff
}

func pegarDataLeituraPedido(order BuscaDePedido.Order) (string,error) {
	var resposta string
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "select top 1 Dat_LeiPed  from pdecb where layout='DANONE' and Cod_PedCli = ?"

	rows, err := db.Query(tsql, fmt.Sprintf("%09s", strconv.Itoa(order.OrderId))) //Layout's fault

	if err != nil {
		logger.Error(
			"PDVCB0001",
			err,
			999,
			"",
			"",
			"checarMudancaStatus",
			"job_buscar_pedidos.go",
		)
	}

	for rows.Next() {
		var resp string
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"PDVCB0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"checarMudancaStatus",
				"job_buscar_pedidos.go",
			)
		}
		resposta = resp
	}
	defer db.Close()

	return resposta, err
}

func checarSeExisteCodPedCli(order BuscaDePedido.Order) (string,bool,error) {
	var resposta string
	hasCodPedCli := true
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "SELECT COUNT(1) FROM pdvcb WHERE Cod_PedCli = ?"

	rows, err := db.Query(tsql, fmt.Sprintf("%09s", strconv.Itoa(order.OrderId))) //Layout's fault

	if err != nil {
		logger.Error(
			"PDVCB0001",
			err,
			999,
			"",
			"",
			"checarMudancaStatus",
			"job_buscar_pedidos.go",
		)
	}

	for rows.Next() {
		var resp int
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"PDVCB0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"checarMudancaStatus",
				"job_buscar_pedidos.go",
			)
		}
		if resp == 0 {
			resposta = "Ordem não existe no sistema"
			hasCodPedCli = false
		}
	}
	defer db.Close()

	return resposta, hasCodPedCli,err
}

func checarNumPedidoVenda(order BuscaDePedido.Order) (string,bool,error) {
	var resposta string
	 hasNumero := true
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "SELECT Numero FROM pdvcb WHERE Cod_PedCli = ?"

	rows, err := db.Query(tsql, fmt.Sprintf("%09s", strconv.Itoa(order.OrderId))) //Layout's fault

	if err != nil {
		logger.Error(
			"PDVCB0001",
			err,
			999,
			"",
			"",
			"checarMudancaStatus",
			"job_buscar_pedidos.go",
		)
	}

	for rows.Next() {
		var resp int
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"PDVCB0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"checarMudancaStatus",
				"job_buscar_pedidos.go",
			)
		}
		if len(strconv.Itoa(resp)) <= 1 {
			resposta = "Núm. Ped. de Venda Inválido"
			hasNumero = false
		}
	}
	defer db.Close()

	return resposta,hasNumero,err
}


package AtualizarStatus

import (
	"bitbucket.org/infarma/layout-danone/BuscaDePedido"
	"bytes"
	"net/http"
	"pedido-eletronico/app/logger"
	"strconv"
	"time"
	"fmt"
	"pedido-eletronico/app/database"
	"strings"
)

func ChecarMudancaStatus(order BuscaDePedido.Order) (string, error) {

	var resposta string
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "select C_DesFase = CASE WHEN cb.Status1 = 'P' THEN CASE WHEN cb.Status2 = 'A' THEN CASE WHEN cb.Cod_PedRem > 0 THEN 'teleprocessado' ELSE 'digitacao' END WHEN cb.Status2 = 'F' THEN CASE WHEN ((cb.Bloqueio = '') or (cb.Bloqueio is null)) AND ((cb.Cod_BlqRnt <> 'B') or (cb.Cod_BlqRnt is null)) AND ((cb.Cod_BlqLic <> 'B') or (cb.Cod_BlqLic is null)) THEN 'em_separacao' ELSE 'bloqueado' END WHEN cb.Status2 = 'N' THEN 'faturado' ELSE '.......' END WHEN cb.Status1 = 'D' THEN 'complete' ELSE 'cancelado' END from dbo.PDVCB as cb inner join dbo.PDECB ecb on ecb.Cod_PedCli = cb.Cod_PedCli where ecb.Cod_PedCli = ?"

	rows, err := db.Query(tsql, fmt.Sprintf("%011s", order.IncrementId)) //Layout's fault

	if err != nil {
		logger.Error(
			"PDVCB0001",
			err,
			999,
			"",
			"",
			"checarMudancaStatus",
			"job_buscar_pedidos.go",
		)
	}

	for rows.Next() {
		var resp string
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"PDVCB0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"checarMudancaStatus",
				"job_buscar_pedidos.go",
			)
		}

		resposta = resp
	}
	defer db.Close()
	return resposta, err
}

func AtualizarStatusDoPedidoSemCriarArq(order BuscaDePedido.Order, status string, url string, token string) {
	client := &http.Client{}
	baseUrl := url + "orders/"

	var jsonStr = []byte(`{"entity": {"entity_id":` + strconv.Itoa(order.OrderId) + `, "increment_id": "` + order.IncrementId + `", "status": "complete","status_histories":[{ "comment":"pedido entregue","parent_id":` + strconv.Itoa(order.StatusHistories[0].ParentId) + `,"is_customer_notified":1,"is_visible_on_front":1,"status":"complete" } ] } }`)
	req, err := http.NewRequest("POST", baseUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		fmt.Println(err)
	}

	authorizationToken := "Bearer " + token
	authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)

	req.Header.Set("Content-type", "application/json")
	req.Header.Add("Authorization", authorizationToken)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Mensagem de retorno da atualização de status do pedido %s: %s - complete\n", order.IncrementId, resp.Status)
}

func AtualizarStatusDoPedidoEnviado(order BuscaDePedido.Order, status string, url string, token string)(bool) {
	var  isAtualizado bool = true
	client := &http.Client{}
	baseUrl := url + "order/" + strconv.Itoa(order.OrderId) + "/ship"
	chaveNotaFiscal,err := buscarChaveNotaFiscal(order)
	if err != nil {
		fmt.Println(err)
	}
	criarStrItens := gerarJSONItem(order)

	var jsonStr = []byte(`{ "items" : [ `+criarStrItens+` ], "notify":true,"appendComment":true,"comment": { "comment": "chave nota fiscal `+chaveNotaFiscal+`","is_visible_on_front":1 } }`)
	req, err := http.NewRequest("POST", baseUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		fmt.Println(err)
	}

	authorizationToken := "Bearer " + token
	authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)

	req.Header.Set("Content-type", "application/json")
	req.Header.Add("Authorization", authorizationToken)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	if resp.StatusCode != 200 {
		isAtualizado = false
	}
	fmt.Printf("Mensagem de retorno da atualização de status do pedido %s: %s - %s\n", order.IncrementId, resp.Status, status)
	return isAtualizado
}


func buscarChaveNotaFiscal(order BuscaDePedido.Order) (string, error){
	var resposta string
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "select NFSCB.Chv_Acesso from PDVCB inner join NFSCB on NFSCB.Cod_Estabe = PDVCB.Cod_Estabe and NFSCB.Ser_Nota = PDVCB.Cod_SerNfs and NFSCB.Num_Nota = PDVCB.Cod_NumNfsIni where PDVCB.Cod_PedCli = ?"

	rows, err := db.Query(tsql,fmt.Sprintf("%09s", strconv.Itoa(order.OrderId)))

	if err != nil {
		logger.Error(
			"NFSCB0001",
			err,
			999,
			"",
			"",
			"buscarChaveNotaFiscal",
			"atualizarStatus.go",
		)
	}

	for rows.Next() {
		var resp string
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"NFSCB0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"buscarChaveNotaFiscal",
				"atualizarStatus.go",
			)
		}

		resposta = resp
	}
	defer db.Close()
	return resposta, err

}

func gerarJSONItem (order BuscaDePedido.Order) (string){
var str = ""
	tamanho := len(order.Items)
	strconv.Itoa(order.OrderId)
	for itemKey, item := range order.Items {
		if tamanho == 1{
			str += "{ \"order_item_id\": " + strconv.Itoa(item.ItemId) + ", "
			str += " \"qty\": " + strconv.Itoa(item.QtyOrdered) + " } "
			return str
		}
			if itemKey != tamanho-1 {
				str += "{ \"order_item_id\": " + strconv.Itoa(item.ItemId) + ", "
				str += " \"qty\": " + strconv.Itoa(item.QtyOrdered) + " }, "
			} else {
				str += "{ \"order_item_id\": " + strconv.Itoa(item.ItemId) + ", "
				str += " \"qty\": " + strconv.Itoa(item.QtyOrdered) + " } "
			}
	}
	return str
}

func ProcessarOrdens (url,token,status string) {
	orders := BuscaDePedido.BuscarPedidosStatus(100, url,token,status)
	if len(orders) > 0 {
		for _, order := range orders {
			if(status == "em_separacao"){
			resp, err := ChecarMudancaStatus(order)
			if err != nil {
				fmt.Println(err)
			}
			if (resp == "complete") { //fluxo completo na Acripel
				isAtualizado := AtualizarStatusDoPedidoEnviado(order, "pedido_enviado", url, token)
				if isAtualizado {
					time.Sleep(time.Second * 1)
					AtualizarStatusDoPedidoSemCriarArq(order, resp, url, token)
				}
			}

		} else if(status == "pedido_enviado"){
				time.Sleep(time.Second * 1)
				AtualizarStatusDoPedidoSemCriarArq(order, "complete", url, token)
			}

			time.Sleep(time.Second * 1) //servidor deles não aguenta muitas requisições.
		}
	}
}


package AtualizarEstoque

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"pedido-eletronico/app/database"
	"pedido-eletronico/app/logger"
	"strconv"
	"strings"
	"time"
)

type Product struct {
	// Campos do De-Para
	Items                []Item    `json:"items"`      // items

}

type Item struct {
	// Campos do De-Para
	Sku               string    `json:"sku"`      // sku
	CustomAttributes []CustomAttributes `json:"custom_attributes"` // custom_attributes
}

type CustomAttributes struct {
	// Campos do De-Para
	AttributeCode      string    `json:"attribute_code"`      // attribute_code
	Value 			   interface{}   `json:"value"`       // value
}

type ProductStockItems struct{
	Qty               int    `json:"qty"`
	ItemId            int    `json:"item_id"`
}

type Kit struct {
	Sku string
	Qtd string
	Ean string
}

func BuscarProdutos(url string, token string, estabe int8) Product {

	resp := FazerRequisicao("products","entity_id","0","gt",9999 ,url, token)

	binData, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println(err)
	}

	var products Product
	rawData := string(binData)

	err = json.Unmarshal([]byte(rawData), &products)
	if err != nil {
		fmt.Println(err)
	}

	return products
}

func FazerRequisicao(model string, searchField string, searchFieldValue string, searchCondition string, pageSize int, url string, token string) *http.Response {
	client := &http.Client{}

	baseUrl        := url

	moduleUrl      := model
	searchCriteria := "?searchCriteria[filterGroups][0][filters][0][field]=" + searchField + "&searchCriteria[filterGroups][0][filters][0][value]=" + searchFieldValue
	conditionType  := "&searchCriteria[filterGroups][0][filters][0][conditionType]=" + searchCondition
	pagination     := "&searchCriteria[pageSize]=" + strconv.Itoa(pageSize)

	fullUrl        := baseUrl + moduleUrl + searchCriteria + conditionType + pagination

	req, err := http.NewRequest("GET", fullUrl, nil)
	if err != nil {
		fmt.Println(err)
	}

	authorizationToken := "Bearer " + token
	authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)

	req.Header.Add("Authorization", authorizationToken)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	return resp
}

func BuscarQtyEItemId(model string, sku string, url string, token string)  *http.Response {
	client := &http.Client{}
	baseUrl        := url
	moduleUrl      := model
	fullUrl        := baseUrl + moduleUrl + sku

	req, err := http.NewRequest("GET", fullUrl, nil)
	if err != nil {
		fmt.Println(err)
	}

	authorizationToken := "Bearer " + token
	authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)
	req.Header.Add("Authorization", authorizationToken)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	return resp
}

func getQtdPorEan(estabe int8,ean string) (int, error){
	var qtd int
	db := database.ConnectMSSqlDB()
	defer db.Close()

	tsql := "SELECT (e.Qtd_Dispon - e.Qtd_Quaren) as Qtd_Est FROM PRXES AS e inner join PRODU AS p ON e.Cod_Produt = p.Codigo left join PREAN ea on ea.Cod_Produt = p.Codigo where e.Cod_Estabe = ? and ((p.Cod_EAN = ?) or (ea.Cod_Ean = ?))"

	rows, err := db.Query(tsql,estabe,ean,ean)

	if err != nil {
		logger.Error(
			"PRXES0001",
			err,
			999,
			"",
			"",
			"getQtdPorEan",
			"prxes.go",
		)
	}

	for rows.Next() {
		var resp int
		err := rows.Scan(&resp)

		if err != nil {
			logger.Error(
				"PRXES0002",
				err,
				999,
				"",
				"Erro ao ler registros",
				"getQtdPorEan",
				"prxes.go",
			)
		}

		qtd = resp
	}
	defer db.Close()
	return qtd, err

}

func atualizarEstoqueMagento (url string, token string, sku string,itemId string ,qtdEstoque string ,codEstab string) string{

	client  := &http.Client{}
	baseUrl := url + "products/" + sku + "/stockItems/" + itemId

		var jsonStr = []byte(`{"stockItem": {"qty":` + qtdEstoque + `}}`)
		req, err := http.NewRequest("PUT", baseUrl, bytes.NewBuffer(jsonStr))
		if err != nil {
			fmt.Println(err)
		}

		authorizationToken := "Bearer " + token
		authorizationToken = strings.Replace(authorizationToken, "\"", "", -1)

		req.Header.Set("Content-type", "application/json")
		req.Header.Add("Authorization", authorizationToken)

		resp, err := client.Do(req)
		if err != nil  {
			fmt.Println("Erro na hora de atualizar estoque no magento: "+ err.Error())

		} else if resp.StatusCode == 200 {
			fmt.Println("Estoque do item com sku:"+sku+"  ,no Estab:"+ codEstab +" ,com nova qtd:"+qtdEstoque+" atualizado.")
		}
	return "-"
}

func ProcessarProdutos(products Product,url string, token string,estab int8 )[][4]string{
	var ProdutosValidados [][4]string
	var dadosProduto [4]string //sku ean item_id qty

	for _, product := range products.Items {
		for _,ean := range product.CustomAttributes {
			if(ean.AttributeCode == "ean"){
				start := time.Now()
				time.Sleep(time.Second * 1) //servidor deles não aguenta muitas requisições.
				resp1 := BuscarQtyEItemId("stockItems/",product.Sku, url, token)
				binData1, err := ioutil.ReadAll(resp1.Body)
				if err != nil {
					fmt.Println(err)
					continue;
				}

				var productSKU ProductStockItems
				rawData1 := string(binData1)
				err = json.Unmarshal([]byte(rawData1), &productSKU)
				if err != nil {
					fmt.Println("Não foi possível atualizar estoque do item de SKU:"+product.Sku+" - Erro: "+ err.Error())
					continue;
				}

				dadosProduto[0] = product.Sku
				dadosProduto[1] = fmt.Sprintf("%v", ean.Value)
				dadosProduto[2] = strconv.Itoa(productSKU.ItemId)
				dadosProduto[3] = strconv.Itoa(productSKU.Qty)

				ProdutosValidados = append(ProdutosValidados,dadosProduto)
				elapsed := time.Since(start)
				fmt.Println("Pegar dados do SKU: "+product.Sku+" , no estab: "+strconv.Itoa(int(estab))+" na DANONE demorou %s",elapsed)
			}
		}

	}

	return ProdutosValidados
}




func ChecarEstoqueBancoEAtualizar(ProdutosValidados[][4]string ,estabe int8,url string, token string, kits []Kit){
	//sku ean item_id qty
	for _, ProdutosValidado := range ProdutosValidados {
		start1 := time.Now()
		ean := ProdutosValidado[1]
		//isKit := false
		qtdKit := 0
		for _,kit := range kits{
			if(ProdutosValidado[0] == kit.Sku){
				ean = kit.Ean
				qtdKit,_ = strconv.Atoi(kit.Qtd)
			}
		}
		qtd,err := getQtdPorEan(estabe,ean)

		if(err != nil){
			println(err)
		}
		if(qtd < 0){
			qtd = 0
		}
		if(qtdKit > 0 && qtd > 0){
			qtd = qtd/qtdKit
		}

		convQtdProdutosValidados,_ := strconv.Atoi(ProdutosValidado[3])

		if(qtd != convQtdProdutosValidados){
			convQtd := strconv.Itoa(qtd)
			convEstab := strconv.Itoa(int(estabe))

			resultado := atualizarEstoqueMagento(url,token,ProdutosValidado[0],ProdutosValidado[2],convQtd,convEstab)
			fmt.Println(resultado)
			elapsed1 := time.Since(start1)
			fmt.Println("Pegar dados do estab: "+strconv.Itoa(int(estabe))+" - de SKU: "+ProdutosValidado[0]+" no Banco Local e Atualizar no magento demorou %s",elapsed1)
			time.Sleep(time.Second * 1) //servidor deles não aguenta muitas requisições.
		}
	}
}
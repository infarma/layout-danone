package arquivoDePedido

import (
	"strconv"
	"strings"
)

// Mensagens (Máximo 2 Registros)
type RegistroTipo02 struct {
	IdentificadorDoTipoDeRegistro int32  `json:"IdentificadorDoTipoDeRegistro"`
	Mensagem                      string `json:"Mensagem"`
}

func GetRegistroTipo2(runes []rune) RegistroTipo02 {
	registroTipo02 := RegistroTipo02{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 64)
	registroTipo02.IdentificadorDoTipoDeRegistro = int32(identificador)

	registroTipo02.Mensagem = strings.TrimSpace(string(runes[2:42]))

	return registroTipo02
}
